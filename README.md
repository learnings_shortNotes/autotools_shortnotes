Auto Tools
============

>Tutorial: [gnu.org](https://www.gnu.org/software/automake/manual/html_node/Headers.html) , [sourceforge](http://inti.sourceforge.net/tutorial/libinti/autotoolsproject.html)


## Makefile.am

- SUBDIRS variable is used to list the subdirectories that must be built
    ```
    SUBDIRS = src
    ```
    `In your top directory give this command in makefile to build those
    folder that you need`

-  bin_PROGRAMS variable specifies that we want a program called 
helloworld to be built and installed in the bin directory when make 
install is run.
    ```
    bin_PROGRAMS = helloworld
    ```
- The AM_CXXFLAGS macro sets the compiler flags.
    ```
    AM_CXXFLAGS = $(INTI_CFLAGS)
    ```
    `You should not use CXXFLAGS in Makefile.am because it's unsafe. 
    CXXFLAGS is a user variable that users expect to be able to 
    override.`
- helloworld_SOURCES variable specifies the source files used to build 
the helloworld target.
    ```
    helloworld_SOURCES = main.cc helloworld.cc helloworld.h
    ```
    ` Note that the SOURCES variable for a target is prefixed by the name 
    of the target, in this case helloworld.`
- helloworld_LDADD, specifies the libraries that must be passed to the 
linker to build the target.
    ```
    helloworld_LDADD = $(INTI_LIBS)
    ```
    `This variable is only used by programs and libraries. Note that 
    LDADD uses the same naming rule as the SOURCES variable.`
- Header files that must be installed are specified by the 
HEADERS family of variables.
    ```
    include_HEADERS = foo.h bar/bar.h
    ```
    `will install the two files as $(includedir)/foo.h and $
    (includedir)/bar.h. `
    ```
    nobase_include_HEADERS = foo.h bar/bar.h
    ```
    `will install the two files as $(includedir)/foo.h and $
    (includedir)/bar/bar.h `
    ```
    noinst_HEADERS = include/nodeserver.h
    ```
    `Used for such headers that are used by programs or 
    convenience libraries which will not be installed.` 
    >**noinst_HEADERS would be the right variable to use in 
    a directory containing only headers and no associated 
    library or program**


## configure.ac
This file must in the project's top-level directory. The list of 
all macros can be found [here](https://www.gnu.org/software/automake/manual/automake.html#Public-Macros)

-  AC_INIT macro performs essential initialization for the generated 
configure script.
    ```
    AC_INIT(packageName, versionNumer)
    ```
    It takes as an argument pachakge name and versionNumber eg: `0.1.0`.
- AM_INIT_AUTOMAKE macro does all the standard initialization required by 
Automake
    ```
    AM_INIT_AUTOMAKE()
    ```
- AC_CONFIG_SRCDIR to set the source file
    ```
    AC_CONFIG_SRCDIR(src/helpMeStudy.c)
    ```
- INTI_REQUIRED_VERSION variable specifies the minimum required 
Inti version
    ```
    INTI_REQUIRED_VERSION=1.0.7
    ```
- PKG_CHECK_MODULES macro checks for the specified version of the 
Inti library
    ```
    PKG_CHECK_MODULES(INTI, inti-1.0 >= $INTI_REQUIRED_VERSION)
    AC_SUBST(INTI_CFLAGS)
    AC_SUBST(INTI_LIBS)
    ```
    `If the Inti library is found, places the necessary include 
    flags in  $(INTI_CFLAGS) and the libraries to link with $
    (INTI_LIBS). If the correct version is not found configure 
    will report an error.`
- AC_PROG_CC checks for the C++ compiler
    ```
    AC_PROG_CC
    ```
    `It sets the variables CXX, GXX and CXXFLAGS.`
- AC_OUTPUT must be called at the end of configure.in to create 
the Makefiles
    ```
    AC_OUTPUT(Makefile src/Makefile)
    ```

## Generating the Output Files
- collect all the macro invocations in configure.in that 
Autoconf will need to build the configure script
    ```
    aclocal
    ```
    `This generates the file aclocal.m4 and adds it to the 
    current directory`
- run autoconf
    ```
    autoconf
    ```
    `[important] Run aclocal first because automake relies 
    on the contents on configure.in and aclocal.m4`
- Files that the GNU standard says must be present in the 
top-level directory, and if not found Automake will report 
an error
    ```
    touch AUTHORS NEWS README ChangeLog
    ```
- run automake to create Makefile.in
    ```
    automake --add-missing
    ```
    `The --add-missing argument copies some boilerplate 
    files from your Automake installation into the current 
    directory.`

At this point you should be able to package up your source tree in a tarball

## Building and Installing the Project
- A user just has to unpack the tarball and run the following 
commands 
    ```
    ./configure --prefix=some_directory
    make
    make install
    ```